# Shadow simulations for Veilid

This is kyanha's repository for setting up [Veilid](https://gitlab.com/veilid/veilid) with
simulations under [Shadow](https://shadow.github.io/).

I intend to make this as self-documenting as possible, but there's going to be some Python
that you have to deal with that I can't actually automate.

Requirements:

- Python 3.11.4+ (the same as Veilid's Python module)
- Shadow 3.2

Process:

1. Create a Python venv. I call mine `venv.dnserver`, because that's what it's going to be
   used for. (Debian 12: You then need to `python -m venv --upgrade venv.dnserver`, or
   else you won't have the `activate` script.

2. Activate the venv. `source ./venv.dnserver/bin/activate`.

3. `pip install -r requirements.txt` to install dnserver 0.4.0.

4. `deactivate` to get out of the virtualenv.


