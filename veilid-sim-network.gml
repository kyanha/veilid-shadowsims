graph [
        directed        0
        node [
                id        0
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        1
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        2
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        3
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        4
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        5
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        6
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        7
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        8
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        9
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        10
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        11
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        12
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        13
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        14
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        15
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        16
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        17
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        18
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        19
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        20
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        21
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        22
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        23
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        24
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        25
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        26
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        27
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        28
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        29
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        30
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        31
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        32
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        33
                label        "bootstrap-1"
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        34
                label        "bootstrap-2"
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        35
                label        "dnsserver"
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        36
                label        "BootstrapSwitch"
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        node [
                id        37
                label        "VeilidPeerSwitch"
                host_bandwidth_down "10 Mbit"
                host_bandwidth_up "10 Mbit"
        ]
        edge [
                source 0
                target 0
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 1
                target 1
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 2
                target 2
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 3
                target 3
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 4
                target 4
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 5
                target 5
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 6
                target 6
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 7
                target 7
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 8
                target 8
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 9
                target 9
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 10
                target 10
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 11
                target 11
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 12
                target 12
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 13
                target 13
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 14
                target 14
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 15
                target 15
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 16
                target 16
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 17
                target 17
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 18
                target 18
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 19
                target 19
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 20
                target 20
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 21
                target 21
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 22
                target 22
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 23
                target 23
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 24
                target 24
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 25
                target 25
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 26
                target 26
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 27
                target 27
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 28
                target 28
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 29
                target 29
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 30
                target 30
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 31
                target 31
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 32
                target 32
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 33
                target 33
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 34
                target 34
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 35
                target 35
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 36
                target 36
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source 37
                target 37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        33
                target        36
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        34
                target        36
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        36
                target        0
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        35
                target        0
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        20
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        8
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        9
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        21
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        22
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        10
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        11
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        23
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        24
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        12
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        32
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        2
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        13
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        25
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        26
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        14
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        1
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        3
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        4
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        5
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        6
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        7
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        15
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        16
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        17
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        18
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        19
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        27
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        28
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        29
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        30
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        31
                target        37
                latency "10 ms"
                packet_loss 0.0
        ]
        edge [
                source        37
                target        0
                latency "10 ms"
                packet_loss 0.0
        ]
]

